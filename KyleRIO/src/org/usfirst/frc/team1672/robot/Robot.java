/*
 * roboRIO Robot Code
 * Designed for 2016 FIRST ROBOTICS COMPETITION
 * Revision 11/16/15
 * 
 * By Kyle Mikolajczyk 
 * 
 * 
 */
package org.usfirst.frc.team1672.robot;


import edu.wpi.first.wpilibj.SampleRobot;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.BuiltInAccelerometer;
import edu.wpi.first.wpilibj.Gyro;
import edu.wpi.first.wpilibj.Jaguar;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.interfaces.Accelerometer;
import edu.wpi.first.wpilibj.vision.AxisCamera;

/**
 * This is a demo program showing the use of the RobotDrive class.
 * The SampleRobot class is the base of a robot application that will automatically call your
 * Autonomous and OperatorControl methods at the right time as controlled by the switches on
 * the driver station or the field controls.
 *
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the SampleRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 *
 * WARNING: While it may look like a good choice to use for your code if you're inexperienced,
 * don't. Unless you know what you are doing, complex code will be much more difficult under
 * this system. Use IterativeRobot or Command-Based instead if you're new.
 */
public class Robot extends SampleRobot 
{
	private boolean kill = false;
	
    Joystick stickLeft, stickRight;
    
    Talon rightDrive = new Talon(0);
	Talon leftDrive = new Talon(1);
	
	Jaguar clawJag = new Jaguar(2);
	Jaguar armJag = new Jaguar(3);
	
	Accelerometer accel = new BuiltInAccelerometer(Accelerometer.Range.k2G);
	RobotDrive myRobot = new RobotDrive(leftDrive, rightDrive);
	
	AxisCamera cam1 = new AxisCamera("10.16.72.2");
	public static AxisCamera.Resolution k640x360;
	
	Gyro gyro = new Gyro(4);
	

    public Robot() 
    {
    	myRobot.setExpiration(0.1);
    	
    	myRobot.setInvertedMotor(RobotDrive.MotorType.kRearLeft, true);
    	myRobot.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
        
        stickLeft = new Joystick(0);
        stickRight = new Joystick(1);
    }

    /**
     * Drive left & right motors for 2 seconds then stop
     */
    public void autonomous()
    {
        myRobot.setSafetyEnabled(false);
        myRobot.drive(-0.5, 0.0);	// drive forwards half speed
        Timer.delay(2.0);		//    for 2 seconds
        myRobot.drive(0.0, 0.0);	// stop robot
    }

    /**
     * Runs the motors with arcade steering.
     */
    public void operatorControl() 
    {
        myRobot.setSafetyEnabled(true);
        while (isOperatorControl() && isEnabled()) 
        {	
            drive.run();
            claw.run();
            arm.run();
            halt.run(); //will be removed, just a test
            
        }
    }
    
    Thread drive = new Thread()
	{
		public void run()
		{
			if(kill == false)
			{
				myRobot.tankDrive(stickLeft, stickRight);
				
				Timer.delay(0.005);		// wait for a motor update time
			}
			else
			{
				myRobot.stopMotor();
			}
		}
	};
	
	Thread claw = new Thread()
	{
		public void run()
		{
			if(stickRight.getTrigger())
			{
				//CloseCLAW
			}
			else
			{
				//release claw code
			}
			
			/*
			 * This is for claw up/down
			 * 
			 */
			if(stickLeft.getRawButton(3)) //3 is UP BUTTON
			{
				clawJag.set(1.0);
			}
			else if(stickLeft.getRawButton(2)) //2 is DOWN BUTTON
			{
				clawJag.set(-1.0);
			}
			else
			{
				clawJag.stopMotor();
			}
		}
	};
	
	Thread arm = new Thread()
	{
		public void run()
		{
			if(stickRight.getRawButton(3)) //3 is UP BUTTON
			{
				armJag.set(-0.5);
			}
			
			if(stickRight.getRawButton(2)) //2 is DOWN BUTTON
			{
				armJag.set(0.5);
			}
			else
			{
				armJag.stopMotor();
			}
		}
	};
	
	Thread halt = new Thread() //Will be removed, want to see if this works
	{
		public void run()
		{
			if(stickRight.getRawButton(4) && stickLeft.getRawButton(5))
			{
				kill = true;
			}
		}
	};

    /**
     * Runs during test mode
     */
    public void test() 
    {
    	System.out.println("****NOW IN TEST MODE!****");
    	myRobot.setSafetyEnabled(false);
    }
}
